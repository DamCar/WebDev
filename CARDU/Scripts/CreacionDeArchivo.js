
window.onload=function()
{
   var path=location.href;
   var camino= path.substring(8,27);
   var nombre=ObtenerVariable("Nombre");
   var Preguntas=[ObtenerVariable("Uno"),ObtenerVariable("Dos"),ObtenerVariable("Tres"),ObtenerVariable("Cuatro"),ObtenerVariable("Cinco"),ObtenerVariable("Seis"),ObtenerVariable("Siete"),ObtenerVariable("Ocho"),ObtenerVariable("Nueve"),ObtenerVariable("Diez")];
    var cuerpo="nombre: "+nombre+" \n";
    for(var a =0;a<10;a++)
        {
            cuerpo+="\n"+(a+1)+"- "+Preguntas[a];
        }
   
    descargarArchivo(generarTexto(cuerpo), 'archivo.txt');
  
}
function descargarArchivo(contenidoEnBlob, nombreArchivo) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var save = document.createElement('a');
        save.href = event.target.result;
        save.target = '_blank';
        save.download = nombreArchivo || 'archivo.dat';
        var clicEvent = new MouseEvent('click', {
            'view':window,
                'bubbles': true,
                'cancelable': false
        });
        save.dispatchEvent(clicEvent);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    };
    reader.readAsDataURL(contenidoEnBlob);
};

function generarTexto(datos) {
    var texto = [];
   
    texto.push(datos);
    
    //El contructor de Blob requiere un Array en el primer parámetro
    //así que no es necesario usar toString. el segundo parámetro
    //es el tipo MIME del archivo
    return new Blob(texto, {
        type: 'text/plain'
    });
};

   


function ObtenerVariable(variable)
{
       var Acomodo = window.location.search.substring(1);
       var vars = Acomodo.split('&');
       for (var i=0;i<vars.length;i++)
       {
               var pair = vars[i].split('=');
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}