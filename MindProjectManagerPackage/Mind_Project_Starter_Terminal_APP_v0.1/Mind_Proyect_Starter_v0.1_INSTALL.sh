#!/bin/bash
#faltan correcciones en el instalador 
amarillo="\e[1;33m"
azul="\e[1;34m"
rojo="\e[1;31m"
verde="\e[0;32m"
cian="\e[0;36m"
o="\e[0m"
BEGIN=$PWD
DOIT=" cp -r $BEGIN/data $BEGIN/MPS /bin/"
function installation
{
	
	cd /bin/
	if [ -e "MPS" ]; 
		then
		echo "MPS ya esta instalado en tu equipo..."
	else
		echo "Instalando MPS..."
		if $DOIT
			then
				mv data .data
				#faltan correcciones en el uninstall 
				echo "MPS instalado con exito "
				echo "Creando UninstallMPS.."
				touch UninstallMPS
				echo "#!/bin/bash">>UninstallMPS
				echo "if [ \$EUID -eq 0 ]; then">>UninstallMPS
				echo "	cd /bin/">>UninstallMPS
				echo "	echo \"Eliminando MPS... \"">>UninstallMPS
				echo "	rm -r .data/ ">>UninstallMPS
				echo "	rm  MPS ">>UninstallMPS
				echo "	rm  UninstallMPS ">>UninstallMPS
				echo "else">>UninstallMPS
				echo "	echo \"Permiso denegado... Intente como superusuario\"">>UninstallMPS
				echo "fi">>UninstallMPS
				chmod +x UninstallMPS
			
		else
			echo "algo salio mal.... intenta como super usuario"
		fi

	fi
} 
if [ "$EUID" -eq 0 ]; then
case $# in
0)
	echo ""
	echo ""
	echo "Se instalaran los siguientes paquetes..."
	cat data/P.txt
	echo ""
	echo "Desea instalarlos ? [S/N]"
	read op
	if [ "$op" == "S" ] || [ "$op" == "s" ]; then
		installation
	fi;;
1)
	case ${1} in
	"-h")
		echo ""
		echo -e "$verde MPSQUARE$o installer"
		echo ""
		echo "Parameters:"
		echo ""
		echo -e "$azul -h$o"
		echo "	Show the help section..."
		echo ""
		echo -e "$azul -v$o"
		echo -e "	Show the Version of the $verde MPSQARE$o installer..."
		echo ""
		echo -e "$azul -l$o"
		echo "	Show the package list and the package code"
		echo ""
		echo -e "$azul -i$o"
		echo "	install the all the packages"
		echo ""
		echo -e "$azul -i$o$amarillo <package code>$o"
		echo "	install the package with the inserted package code"
		echo ""
		;;
	"-v")
		echo ""
		echo $(head -n 1 data/v.txt)
		echo "";;
	"-l")
		echo ""
		cat data/P.txt
		echo "";;
	"-i")
		echo ""
		echo ""
		echo "Se instalaran los siguientes paquetes..."
		cat data/P.txt
		echo ""
		echo "Desea instalarlos ? [S/N]"
		read op
		if [ $op = "S" ] || [ $op ="s" ]; then
			installation
		fi;;
		
	esac;;	
2)
	;;
*)
	;;
esac
cd $BEGIN
else
echo ""
echo "Permiso Denegado... intente como superusuario"
echo ""
fi
