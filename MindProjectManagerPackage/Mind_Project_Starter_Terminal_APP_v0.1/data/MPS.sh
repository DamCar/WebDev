#!/bin/bash
# MPSv0.01  1/junio/2017  
echo "Creando arbol de directorios  del proyecto ${1}..."
mkdir ${1}
cd ${1}
#Generacion de Archivos iniciales o index

echo "Creando index (haml y html)..."
echo "Configurando index (haml y html)..."
touch index.haml
echo "!!!html">>index.haml
echo "%html">>index.haml
echo "	%head">>index.haml
echo "		%title">>index.haml
echo "			${1}-INICIO">>index.haml
echo "		%meta{:charset=>\"utf-8\"}">>index.haml
echo "		%meta{:lang=>\"es\"}">>index.haml
echo "		%meta{:name=>\"viewport\",:content=>\"width=device-width, initial-scale=1.0\"}">>index.haml
echo "		%link{:rel=>\"stylesheet\",:href=>\"css/style.css\"}">>index.haml
echo "		%script{:type=>\"text/javascript\",:src=>\"js/${1}Sample.js\"}">>index.haml
echo "	%body">>index.haml
echo "		%h1">>index.haml
echo "			${1}Sample">>index.haml
cat index.haml
echo "creando fichero html a partir de fichero haml"
haml index.haml index.html
cat index.html
echo "index (haml y html) configurado..."

#Generacion de directorios
echo "Creando carpeta de assets..."
mkdir assets 
#Luego pondre un comando para incluir assets 

#creacion de Scripts
echo "Creando carpeta de javaScriṕt (js)..."
mkdir js 
cd js
touch ${1}Sample.js
echo "alert(\"${1}: Alerta de Prueba!!\");">>${1}Sample.js
cd ..
#Creacion de la carpeta de estilos y el primer css
echo "Creando carpeta de estilos (css)..."
mkdir css 
echo "Generando estilo inicial..."
cd css
touch style.css
echo "*{">>style.css
echo "	margin:0;">>style.css
echo "	padding:0;">>style.css
echo "	background:rgb(0,0,0);">>style.css
echo "}">>style.css
echo "h1{">>style.css
echo "	text-shadow:0em 0em 0.25em rgb(120,120,120);">>style.css
echo "	text-align:center;">>style.css
echo "	margin:1em;">>style.css
echo "	padding:0.5em;">>style.css
echo "	color:#36BED2;">>style.css
echo "}">>style.css
cd ..
echo "Generando README.md Del proyecto..."
touch README.md
DIA=`date +"%d/%m/%y"`
echo "generando registro a la fecha de: $DIA"
echo "#${1}">>README.md
echo "## Fecha de Inicio $DIA">>README.md
echo "listo! "
